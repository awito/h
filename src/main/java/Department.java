import java.util.*;

/**
 * A department class with list to hold employees and patints
 */


public class Department {

    private String departmentName;
    private HashMap<String, Person> persons;
    private Patient patient;
    private RemoveException exception;

    /**
     * Initializes department object
     * @param departmentName
     */
    public Department(String departmentName){

        this.departmentName = departmentName;
        this.persons = new HashMap<String, Person>();
    }


    /**
     * sets department name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }


    /**
     * gets department name
     * @return String
     */

    public String getDepartmentName() {
        return departmentName;
    }



    /**
     * adds Employee to the
     * @param ssn
     * @param employee
     */

    public void addEmployee(String ssn, Employee employee) {
        if (employee != null) {

                this.persons.put(ssn, employee);


        }
    }



    /**
     * returns list of employees
     * @return list
     */

    public List<Employee> getEmployees() {
        List<Employee> employeeList = new ArrayList<>();
        for (Person person : this.persons.values()) {
            if (person instanceof Employee) {

                employeeList.add((Employee) person);

            }
        }

       return  employeeList;
    }



    /**
     * returns list of patients
     * @return list
     */

    public List<Patient> getPatient(){
        List<Patient> patientList = new ArrayList<>();
        for (Person person : this.persons.values()) {
            if (person instanceof Patient) {

               patientList.add((Patient) person);
            }
        }

        return  patientList;
    }


    /**
     * adds patient to the map
     * @param ssn
     * @param patient
     */

    public void addPatient(String ssn, Person patient){
        if (patient != null) {

                this.persons.put(ssn,patient);
        }
    }


    /**
     * A method to remove a person from the map
     * @param ssn
     */

    public void removePerson(String ssn) throws RemoveException {
        if (ssn == null) {
            throw new IllegalArgumentException();
        } else {
            for (Person per : persons.values()) {
                if (per.getSsn() == ssn) {
                    this.persons.remove(per);
                }
            }
        }

        throw new RemoveException("Person Not Found!");
    }



    /**
     * method to compare to objects
     * @param o
     * @return true if objects are equal, false if objects are either null or not equal.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return persons.equals(that.persons);
    }


    /**
     * generates hashcode for an object
      * @return
     */

    @Override
    public int hashCode() {
        return Objects.hash(persons);
    }



    /**
     * details of a department
     * @return String
     */

    public String toString(){
        return this.getDepartmentName()+ "Number of patients: " + getPatient().size() +"\n Numbers of personals: " + getEmployees().size();
    }


    public int size(){
       return this.persons.size();
    }
}


