/**
 * An interface
 */

public interface Diagnosable {

    void setDiagnosis(String diagnosis);
}
