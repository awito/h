

/**
 * Class to hold the objects of a doctor
 * it extends Employee
 */


public abstract class Doctor extends Employee {

    private String fName;
    private String lName;
    private String ssn;


    /**
     * Initializes doctor object
     * @param fName
     * @param lName
     * @param ssn
     */
    public Doctor(String fName, String lName, String ssn){
        super(fName,lName,ssn);
    }


    /**
     * returns the details of a doctor
     * @return String
     */
    @Override
    public String toString(){
        return this.fName+""+this.lName+""+this.ssn;
    }
}
