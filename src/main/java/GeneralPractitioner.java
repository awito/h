/**
 * A class to hold objects of generalPractitioner
 */


public class GeneralPractitioner extends Doctor{


    private String fName;
    private String lName;
    private String ssn;



    /**
     * Initializes generalPractitioner object
     * @param fName
     * @param lName
     * @param ssn
     */
    public GeneralPractitioner(String fName, String lName, String ssn){
        super(fName,lName,ssn);
    }

    public void setDiagnosis(){

    }


}
