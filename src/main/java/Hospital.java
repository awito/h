import java.util.ArrayList;
import java.util.List;

public class Hospital {

    private String Hname;
    private ArrayList<Department> departments;


    /**
     * Initialize a hospital
     * @param name
     */

    public Hospital(String name){
        Hname = name;
        departments = new ArrayList<>();
    }

    /**
     * A method to get a hospital name
     * @return
     */

    public String getHospitalName() {
        return Hname;
    }

    /**
     * A method to get all departments
     * @return List
     */

    public List<Department> getDepartments(){

        if(departments == null){
        }

        List<Department> dep = new ArrayList<>();
        for (Department department: departments) {
            dep.add(department);
        }

        return dep;
    }

    /**
     * A method to add a department to the list
     * @param department
     */

    public void addDepartment(Department department){
        if (department == null)
        {

        }
        departments.add(department);
    }


    /**
     * A method that returns details of a hospital
     * @return String
     */

    public String toString(){
        return this.Hname + "Number of departments \n " + getDepartments().size();
    }
}
