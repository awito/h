


/**
 * A class to hold the objects of a nurse
 */


public class Nurse extends Employee {

    private String fName;
    private String lName;
    private String ssn;



    /**
     * Initializes doctor object
     * @param fName
     * @param lName
     * @param ssn
     */
    public Nurse(String fName, String lName, String ssn){
        super(fName,lName,ssn);
    }

    /**
     * returns the details of a nurse
     * @return String
     */
    @Override
    public String toString(){
        return this.fName+""+this.lName+""+this.ssn;
    }

}
