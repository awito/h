/**
 * A class to hold patient object
 * That extends person class and implements diagnosable interface
 */

public class Patient extends Person implements Diagnosable {


    private String diagnosis;



    public Patient(){
        super();
        diagnosis = "";
    }

    /**
     * Initializes surgeon object
     * @param fName
     * @param lName
     * @param ssn
     */
    public Patient(String fName, String lName, String ssn, String diagnosis){
        super(fName,lName,ssn);

        if(diagnosis == null){
            this.diagnosis = "";
        }
        else {
            this.diagnosis = diagnosis;
        }
    }


    /**
     * Implements setDiagnosis method from diagnosable interface
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis){
      this.diagnosis = diagnosis;
    }

    /**
     * returns details of patient
     * @return string
     */
    @Override
    public String toString(){
        return
                super.toString() +this.diagnosis;
    }


}
