/**
 * An abstract class to hold common details for persons
 */


public abstract class Person {


    private String fName;
    private String lName;
    private String ssn;





    public Person(String fName, String lName, String ssn) {
        if (fName == null){
            fName = "";
        }
        else {

            this.fName = fName;
        }

        if (lName == null){
            lName = "";
        }
        else {

            this.lName = lName;
        }

        if (ssn == null){
           this.ssn = "";
        }
        else {

            this.ssn = ssn;
        }

    }

    /**
     * Empty initializer
     */
    public Person() {

    }



    /**
     *
     * @return String
     */

    public String getfName() {
        return fName;
    }

    /**
     * sets persons first name
     * @param fName
     */

    public void setfName(String fName) {
        this.fName = fName;
    }

    /**
     * gets persons last name
     * @return String
     */

    public String getlName() {
        return lName;
    }


    /**
     * sets persons last name
     * @param lName
     */

    public void setlName(String lName) {
        this.lName = lName;
    }

    /**
     * gets persons social security number
     * @return String
     */
    public String getSsn() {
        return ssn;
    }


    /**
     * sets persons social security number
     * @param ssn
     */

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }


    /**
     * gets persons full name
     * @return string
     */
    public String getFullName(){
        return fName + lName;
    }


    /**
     * returns details of person
     * @return string
     */

    public String toString(){
        return fName+ ""+lName+""+ssn;
    }
}
