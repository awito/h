


public class RemoveException extends Exception {


    /**



    private static Department department;
    private static Person person;

    public RemoveException(Department dep, Person per){

        department = dep;
        person = per;
    }

    public static void main(String[] args) {


        try {

            department.removePerson(person);
        }
        catch (RemoveException e){
            System.out.println("Person Not Found");
        }

    }
     */

    public RemoveException(String message){
        super(message);
    }

}
