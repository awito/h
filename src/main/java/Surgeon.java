


/**
 * A class to hold he objects of a surgeon
 */


public class Surgeon extends Doctor{

    private String fName;
    private String lName;
    private String ssn;



    /**
     * Initializes surgeon object
     * @param fName
     * @param lName
     * @param ssn
     */
    public Surgeon(String fName, String lName, String ssn){
        super(fName,lName,ssn);
    }

    public void setDiagnosis(){

    }
}
